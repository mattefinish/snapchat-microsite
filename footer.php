<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</div>

	<footer class="footer">
        <div class="container">

            <!-- This Is Matte -->
            
            <div class="this_is_matte">
                <span>This is <b><a href="http://matteprojects.com">A Matte</a></b> Project</span>
                <ul class="films_music">
                    <li><a href="<?php the_field('films_link', 'option'); ?>" target="_blank">Films</a></li>
                    <li><a href="<?php the_field('music_link', 'option'); ?>" target="_blank">Music</a></li>
                </ul>
            </div>

            <!-- Newsletter -->

            <div class="email_subscribe">
                <div id="mc_embed_signup">
                    <form action="//COM.us4.list-manage.com/subscribe/post?u=d60a686aa680084badf128c9d&amp;id=4003d5d709" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <label for="mce-Email"><b>Join Our Mailing List:</b></label>
                                <input type="email" value="" name="EMAIL" class="required email" placeholder="" id="mce-EMAIL">
                                <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Contact & Social -->

            <div class="footer_contact">
                <?php get_template_part('templates/social_media'); ?>
                <div class="address">
                    <?php the_field('footer_text', 'option'); ?>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php wp_footer(); ?>

<!-- Google Analytics -->

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-79341802-1']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

</body>
</html>
