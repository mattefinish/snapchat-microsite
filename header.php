<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="<?php the_field('matte_favicon', 'option'); ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<header class="site_header" role="banner">
		<div class="logo">
			<a class="logo_image" href="http://matteprojects.com">
				<img src="<?php the_field('matte_black_logo','option'); ?>" />
			</a>
			<ul class="films_music">
                <li><a href="<?php the_field('films_link', 'option'); ?>" target="_blank">Films</a></li>
                <li><a href="<?php the_field('music_link', 'option'); ?>" target="_blank">Music</a></li>
            </ul>
		</div>
		<nav class="main_navigation" role="navigation">
			<ul>
				<li><a href="#homepage_about">About</a></li>
				<li><a href="#clients">Clients</a></li>
				<li class="connect">
					<span class="connect_text">Connect</span>
					<?php get_template_part('templates/social_media'); ?>
				</li>
			</ul>
		</nav>
	</header>

	<!-- Mobile Navigation -->

    <div class="mobile_navigation">

        <div class="mobile_header">
            <div class="mobile_header_container">
                <div class="mobile_logo_container"> 
                    <a href="http://matteprojects.com"><img src="<?php the_field('matte_black_logo','option'); ?>"></a>
                </div>
                <div class="close_menu_icon"></div>
            </div>
        </div>
        
        <div class="content_container">
            <ul class="mobile_menu">
                <li><a href="#homepage_about">About</a></li>
				<li><a href="#clients">Clients</a></li>
            </ul>
            <?php get_template_part('templates/social_media'); ?>
            <ul class="music_films_container">
                <li><a href="<?php the_field('films_link', 'option'); ?>" target="_blank">Films</a></li>
                <li><a href="<?php the_field('music_link', 'option'); ?>" target="_blank">Music</a></li>
             </ul>
        </div>  

    </div>

    <!-- Mobile Header -->

    <div class="main_mobile_header">
        <div class="mobile_header">
            <div class="mobile_header_container">
                <div class="mobile_logo_container"> 
                    <a href="http://matteprojects.com"><img src="<?php the_field('matte_black_logo','option'); ?>"></a>
                </div>
                <div class="menu_icon"></div>
            </div>
        </div>
    </div>

	<div id="content" class="main_content">