var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	/*================================= 
	HEADER
	=================================*/

	/*=== Scrolled Header ===*/

	$j(window).scroll(function(){
	   var scroll = $j(this).scrollTop();
	   var pageContent = $j('.page_content').offset().top;
	   if (scroll > pageContent) {
	   		$j('.site_header').addClass('scrolled');
	   }
	   else {
	   		$j('.site_header').removeClass('scrolled');
	   }
	});

	/*=== Logo ===*/

    $j('.site_header .logo').hover(function(){
      $j('.site_header .films_music').toggleClass('visible')
    })

    /*=== Connect Social Icons ===*/

    $j('.connect').hover(function(){
    	$j(this).toggleClass('active');
    });

	/*================================= 
	SMOOTH SCROLL
	=================================*/

	/*=== Navigation Links ===*/

	$j('.main_navigation a').smoothScroll({
		speed: 1000
	});

	/*=== Mobile Navigation Links ===*/

	$j('.mobile_menu a').smoothScroll({
		speed: 1000
	});

	/*=== Down Arrow ===*/

	$j('.down_arrow a').smoothScroll({
		speed: 1000,
		offset: -60
	});

	/*======================== 
	MOBILE NAVIGATION
	========================*/

	/*=== Open the fullscreen overlay ===*/

	$j('.menu_icon').click(function(){
		$j('.mobile_navigation').fadeIn('fast');
	});

	/*=== Clicking the x within the overlay closes it ===*/

	$j('.close_menu_icon, .mobile_menu a').click(function(){
		$j('.mobile_navigation').fadeOut('fast');
	});

	/*================================= 
	MASONRY CAMPAIGN GRID
	=================================*/

	/*=== On click of the more button change the text to 'loading' ===*/

	$j('.alm-load-more-btn.loading').on('click tap', function(){
		$j(this).text('loading');
	});

	// Set up an empty array to log the ALM pages

	var almPages = [];

	var masonryInit = true;

	/*=== Ajax Load More callback function. Run after each succesful AJLM query ===*/

	$j.fn.almComplete = function(alm){

		// ALM page number for this specific container

		var pageNumber = alm.page;

		// If the page isn't found in our pages array then initialize flexslider
		// Doing this so flexslider isn't initialized for multiple page callings.

		if(almPages.indexOf(pageNumber) === -1) {
			$j('.campaign_slider').flexslider({
				directionNav: false
			});
		}

		// Add the page to our array so we know it already exists on the page

		almPages.push(pageNumber);

		/*=== Insert the sizing elements for masonry within the ajax load more container ===*/

		$j("ul.alm-listing").append('<div class="campaign_grid_sizer"></div> <div class="campaign_grid_gutter"></div>');

		/*=== Set the container where our campaigns exist ===*/

		var $container = $j('#campaign_grid ul.alm-listing');

		/*=== Initialize masonry once ===*/

		if(masonryInit){
			masonryInit = false;
			$container.masonry({
			  itemSelector: '.campaign_grid_item',
			  columnWidth: '.campaign_grid_sizer',
			  gutter: '.campaign_grid_gutter',
			  percentPosition: true
			});
		}

		/*=== If already initialized then reload items in masonry ===*/

		else {
			$container.masonry('reloadItems');
		}

		/*=== When images are loaded, fire masonry again. Wait a few seconds to fade in the items so they don't slide from the top ===*/

		$container.imagesLoaded( function() { 
			$container.masonry();
			$j('.campaign_grid_item').delay(0).fadeTo(100,1);
		});

		// Once the video has ended then reload it, show the first frame and show the play button

		$j('video').on('ended', function(){
			$j(this).get(0).load(0);
			$j(this).closest('.video_container').find('.video_controls').removeClass('playing hide');
		});

		// Once the grid is done loading then reset the text to 'more' 

		$j('.alm-load-more-btn').text('more');

	};

	/*=== AJAX Load More callback when it is done loading posts ===*/

	$j.fn.almDone = function(){
      $j('.alm-load-more-btn').text('more');
   	}; 

	/*================================= 
	VIDEO CONTROLS
	=================================*/

	// Click event for video controls

	$j('body').on('click', '.video_controls', function(){

		var closestVideo = $j(this).closest('.video_container').find('video').get(0);

		// Detect if the video is playing or paused

		if(closestVideo.paused) {
			closestVideo.play();
			$j(this).addClass('playing');
		}

		else {
			closestVideo.pause();
			$j(this).removeClass('playing');
		}

	});

	// Hover function to show or hide the controls

	$j('body').on('hover', '.video_container', function(){
		var closestVideo = $j(this).find('video').get(0);

		// Detect if the video is playing. If playing then show/hide the controls on hover.

		if(!closestVideo.paused) {
			$j(this).find('.video_controls').toggleClass('hide');
		}
	});

	// Exit out of the lightbox and pause the video

	$j('body').on('click', '.close_icon', function(){
		
		// Only trigger this if the lightbox has a video

		if($j(this).closest('.lightbox').find('video').length) {
			$j(this).closest('.lightbox').find('video').get(0).pause();
			$j(this).closest('.lightbox').find('.video_controls').removeClass('playing hide');
		}
	});

	/*================================= 
	GIF HOVER LAZY LOAD
	=================================*/

	$j('#campaign_grid').on('hover','.campaign_grid_item', function(){

		var gif = $j(this).find('img.campaign_gif');
		var imageSRC = gif.attr('data-src');
		if(gif.length && gif[0].hasAttribute('data-src')) {
			gif.attr('src', imageSRC);
			gif.removeAttr('data-src');
		}

	});

	/*================================= 
	LIGHTBOX
	=================================*/

	/*=== Open Individual Campaign Lightbox ===*/

	$j('#campaign_grid').on('click','.campaign_grid_item', function(){
		var campaignName = $j(this).attr('data-campaign');

		// Lazy load lightbox images

		$j('.lightbox#' + campaignName).find('img').each(function(){
			var imageSRC = $j(this).attr('data-src');
			$j(this).attr('src', imageSRC);
		});

		//  Open the specific lightbox
		
		$j('.lightbox#' + campaignName).addClass('open');
	});

	/*=== Close the popup ===*/

	$j('#campaign_grid').on('click tap','.close_icon', function(){
		$j(this).closest('.lightbox').removeClass('open');
	});

	/*================================= 
	LOAD MORE BUTTON
	=================================*/

	$j('.alm-load-more-btn.loading').click(function(){
		$j(this).text('loading');
	});

});