<ul class="social">
    <li><a href="<?php the_field('matte_facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
    <li><a href="<?php the_field('matte_instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
    <li><a href="<?php the_field('matte_twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
    <li><a href="<?php the_field('matte_vimeo', 'option'); ?>" target="_blank"><i class="fa fa-vimeo"></i></a></li>
    <li><a href="mailto:<?php the_field('matte_email', 'option'); ?>" target="_blank"><i class="fa fa-envelope-o"></i></a></li>
</ul>