<?php
	// Individual campaign content. This template runs for each campaign as ajax load more calls them into the DOM.
	// Set the variable for campaign name so the grid item has a reference for its associated lightbox.
	global $post;
	$campaign_name_slug = $post->post_name;
?>

<?php
// The grid thumbnail. Contains the thumbnail, gif and client logo.
// Video campaigns have the unique gif hover. Geofilters zoom in on hover
?>

<?php 
// Video
if(get_field('campaign_type') === 'video'): ?>
	<a class="campaign_grid_item" data-campaign="<?php echo $campaign_name_slug; ?>">
		<?php the_post_thumbnail('large', array('class' => 'campaign_thumb')); ?>
		<?php if(get_field('campaign_logo')): ?>
			<div class="campaign_logo">
				<div class="logo_container">
					<img src="<?php the_field('campaign_logo'); ?>" />
				</div>
			</div>
		<?php endif; ?>
		<?php 
			$imageID = get_field('campaign_gif'); 
			$imageObject = wp_get_attachment_image_src($imageID, 'full');
			$imageSRC = $imageObject[0];
		?>
		<div class="campaign_grid_overlay">
			<img class="campaign_gif" data-src="<?php echo $imageSRC; ?>" src="" />
		</div>
	</a>
<?php 
// Geofilter
elseif(get_field('campaign_type') === 'geofilter'): ?>
	<a class="campaign_grid_item geofilter" data-campaign="<?php echo $campaign_name_slug; ?>">
		<?php the_post_thumbnail('large', array('class' => 'campaign_thumb')); ?>
		<?php if(get_field('campaign_logo')): ?>
			<div class="campaign_logo">
				<div class="logo_container">
					<img src="<?php the_field('campaign_logo'); ?>" />
				</div>
			</div>
		<?php endif; ?>
		<div class="campaign_grid_overlay"></div>
	</a>
<?php endif; ?>

<?php // The individual campaign lightbox. ?>

<div class="lightbox" id="<?php echo $campaign_name_slug; ?>">

	<?php // Title Panel ?>

	<section class="title_panel with_padding">
		<div class="content">
			<?php if(get_field('alternative_title')): ?>
				<h1><?php the_field('alternative_title'); ?></h1>
			<?php else: ?>
				<h1><?php the_title(); ?></h1>
			<?php endif; ?>
			<?php if(get_field('campaign_descriptor')): ?>
				<p><?php the_field('campaign_descriptor'); ?></p>
			<?php endif; ?>
		</div>
	</section>

	<?php // Video or Image Panel ?>

	<section class="media_panel">
		<?php if(get_field('campaign_type') === 'video'): ?>
			<div class="video_container">
				<video poster="<?php the_field('video_cover_image'); ?>" preload="none">
					<source src="<?php the_field('campaign_video'); ?>" type="video/mp4">
				</video>
				<div class="video_controls">
					<i class="fa fa-play"></i>
					<i class="fa fa-pause"></i>
				</div>
			</div>
		<?php elseif(get_field('campaign_type') === 'geofilter'): ?>
			<?php if(have_rows('geofilter_slideshow')): ?>
				<div class="flexslider campaign_slider">
					<ul class="slides">
						<?php while(have_rows('geofilter_slideshow')): the_row(); ?>
							<li>
								<div class="image_container">
									<img data-src="<?php the_sub_field('geofilter_slideshow_image'); ?>" src="" />
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
			<?php else: ?>
				<div class="image_container">
					<img data-src="<?php the_field('geofilter_lightbox_image'); ?>" src="" />
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</section>

	<?php // Campaign Details Panel ?>

	<section class="detials_panel with_padding">
		<div class="details_container">
			<?php if(get_the_content()): ?>
				<div class="details_info">
			<?php else: ?>
				<div class="details_info no_excerpt">
			<?php endif; ?>
			
				<?php 
					// Clients
					$clients = get_the_terms($post->ID, 'client');
					if($clients):
					$client_name = $clients[0]->name;
				?>
					<h3>Client</h3>
					<p><?php echo $client_name; ?></p>
				<?php endif; ?>

				<?php if(get_field('campaign_launch')): ?>
					<h3>Launch</h3>
					<p><?php the_field('campaign_launch'); ?></p>
				<?php endif; ?>

				<?php 
					// Services
					$services = get_the_terms($post->ID, 'services');
					if($services):
				?>
					<h3>Services</h3>
					<p>
						<?php
							$services_arr = array();
							foreach($services as $service) {
								$services_arr[] = $service->name;
							}
							echo implode(', ', $services_arr);
						?>					
					</p>
				<?php endif; ?>
			</div>
			<?php if(get_the_content()): ?>
				<div class="details_excerpt">
					<?php the_content(); ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
	<div class="close_icon"></div>
</div>