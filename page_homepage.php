<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Top Banner with Snapchat Gif -->

			<header class="main_banner">
				<div class="featured_gif">
					<?php the_post_thumbnail(); ?>
				</div>
				<div class="down_arrow">
					<a href="#campaign_grid"><i class="fa fa-angle-down"></i></a>
				</div>
			</header>

			<div class="page_content">

				<!-- About -->

				<section id="homepage_about" class="page_content_container">
					<?php the_content(); ?>
				</section>

				<!-- Campaigns -->

				<section id="campaign_grid">
					<div class="container">
						<?php echo do_shortcode('[ajax_load_more theme_repeater="alm-campaign-grid.php" post_type="matte_campaign" orderby="menu_order" order="ASC" posts_per_page="7" scroll="false" button_label="more" transition="fade"]'); ?>
					</div>
				</section>

				<!-- Clients -->

				<section id="clients">
					<div class="container">
						<h1 class="center white">Clients</h1>
						<ul class="client_list">
							<?php 
								$clients = get_terms( 'client', array('hide_empty' => false) );
								foreach($clients as $client):
							?>
								<li><?php echo $client->name ?></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</section>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
